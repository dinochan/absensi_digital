-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 08, 2021 at 04:33 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `absensi`
--

-- --------------------------------------------------------

--
-- Table structure for table `absensi`
--

CREATE TABLE `absensi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bulan` int(11) DEFAULT NULL,
  `id_team` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `absensi`
--

INSERT INTO `absensi` (`id`, `date`, `bulan`, `id_team`, `created_at`, `updated_at`) VALUES
(674, 'Thursday, 01 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(675, 'Friday, 02 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(676, 'Saturday, 03 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(677, 'Sunday, 04 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(678, 'Monday, 05 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(679, 'Tuesday, 06 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(680, 'Wednesday, 07 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(681, 'Thursday, 08 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(682, 'Friday, 09 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(683, 'Saturday, 10 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(684, 'Sunday, 11 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(685, 'Monday, 12 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(686, 'Tuesday, 13 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(687, 'Wednesday, 14 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(688, 'Thursday, 15 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(689, 'Friday, 16 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(690, 'Saturday, 17 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(691, 'Sunday, 18 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(692, 'Monday, 19 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(693, 'Tuesday, 20 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(694, 'Wednesday, 21 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(695, 'Thursday, 22 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(696, 'Friday, 23 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(697, 'Saturday, 24 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(698, 'Sunday, 25 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(699, 'Monday, 26 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(700, 'Tuesday, 27 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(701, 'Wednesday, 28 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(702, 'Thursday, 29 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45'),
(703, 'Friday, 30 April 2021', 4, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', '2021-04-02 01:16:45', '2021-04-02 01:16:45');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gaji`
--

CREATE TABLE `gaji` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `komponen` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `ket` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `norek` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gaji`
--

INSERT INTO `gaji` (`id`, `komponen`, `user_id`, `total`, `ket`, `norek`, `bank`, `created_at`, `updated_at`) VALUES
(27, 'gapok', 11, 1000000, 'gaji', NULL, NULL, '2021-04-06 06:30:34', '2021-04-06 06:30:34');

-- --------------------------------------------------------

--
-- Table structure for table `gaji_cuti`
--

CREATE TABLE `gaji_cuti` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tgl_mulai_cuti` datetime DEFAULT NULL,
  `tgl_akhir_cuti` datetime DEFAULT NULL,
  `status` int(11) DEFAULT 0,
  `gaji` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `hari` int(11) DEFAULT NULL,
  `keperluan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gaji_cuti`
--

INSERT INTO `gaji_cuti` (`id`, `tgl_mulai_cuti`, `tgl_akhir_cuti`, `status`, `gaji`, `user_id`, `hari`, `keperluan`, `created_at`, `updated_at`) VALUES
(5, '2021-04-01 00:00:00', '2021-04-28 00:00:00', 1, 120000, 11, 2, '', '2021-04-06 06:31:36', '2021-04-06 06:31:36');

-- --------------------------------------------------------

--
-- Table structure for table `gaji_lembur`
--

CREATE TABLE `gaji_lembur` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `jam_masuk_lembur` time DEFAULT NULL,
  `jam_keluar_lembur` time DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gaji` int(11) DEFAULT NULL,
  `id_team` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jam_kerja`
--

CREATE TABLE `jam_kerja` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_team` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jam_masuk` time DEFAULT NULL,
  `jam_keluar` time DEFAULT NULL,
  `telat` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kerja`
--

CREATE TABLE `kerja` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `bulan` int(11) DEFAULT NULL,
  `lembur` int(11) DEFAULT NULL,
  `absen` time DEFAULT NULL,
  `lembur_at` time DEFAULT NULL,
  `absen_telat` int(11) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `langitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note_radius_masuk` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note_radius_keluar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cuti` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kerja`
--

INSERT INTO `kerja` (`id`, `date`, `status`, `user_id`, `created_at`, `updated_at`, `bulan`, `lembur`, `absen`, `lembur_at`, `absen_telat`, `image`, `longitude`, `langitude`, `note`, `note2`, `note_radius_masuk`, `note_radius_keluar`, `cuti`) VALUES
(67, 'Thursday, 01 April 2021', 2, 11, '2021-04-06 06:30:44', '2021-04-06 06:32:09', 4, NULL, '13:30:44', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_01_21_154556_create_absensi_table', 1),
(5, '2021_01_24_224944_create_kerja_table', 2),
(6, '2021_01_25_200811_create_gaji_table', 3),
(7, '2021_01_25_205349_create_potongan_table', 4),
(8, '2021_04_04_200032_create_jam_kerja_table', 5),
(9, '2021_04_04_205313_create_gaji_lembur_table', 6),
(10, '2021_04_05_193801_create_gaji_cuti_table', 7),
(11, '2021_04_05_194636_create_role_pay_table', 8),
(12, '2021_04_05_194715_create_role_payment_table', 8),
(13, '2021_04_06_221449_add_google_to_users_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `potongan`
--

CREATE TABLE `potongan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `komponen` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `ket` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `potongan`
--

INSERT INTO `potongan` (`id`, `komponen`, `user_id`, `total`, `ket`, `created_at`, `updated_at`) VALUES
(15, 'gatot', 11, 1000, 'potongan', '2021-04-06 06:30:34', '2021-04-06 06:30:34');

-- --------------------------------------------------------

--
-- Table structure for table `role_pay`
--

CREATE TABLE `role_pay` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `bulan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `harga` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_pay`
--

INSERT INTO `role_pay` (`id`, `user_id`, `bulan`, `harga`, `created_at`, `updated_at`) VALUES
(16, NULL, '1', '23333', '2021-04-05 13:23:24', '2021-04-05 13:23:24'),
(17, NULL, '2', '4343343', '2021-04-05 13:23:24', '2021-04-05 13:23:24'),
(18, NULL, '32', '4343', '2021-04-05 13:23:39', '2021-04-05 13:23:39');

-- --------------------------------------------------------

--
-- Table structure for table `role_payment`
--

CREATE TABLE `role_payment` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `dibayar` datetime DEFAULT NULL,
  `tgl_bayar` datetime DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `bank` int(11) DEFAULT NULL,
  `pay` int(11) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_read` int(11) DEFAULT NULL,
  `bulan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_payment`
--

INSERT INTO `role_payment` (`id`, `user_id`, `dibayar`, `tgl_bayar`, `harga`, `bank`, `pay`, `image`, `is_read`, `bulan`, `created_at`, `updated_at`) VALUES
(1, 11, '2021-06-02 00:00:00', '2021-04-06 23:17:36', 4343343, NULL, 2, '163616546.png', NULL, '2', '2021-04-06 16:17:36', '2021-04-06 16:17:36'),
(2, 13, '2021-01-04 20:56:44', NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_team` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `google_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `image`, `password`, `remember_token`, `id_team`, `level`, `created_at`, `updated_at`, `google_id`, `longitude`, `latitude`) VALUES
(10, 'dino', 'teling@gmail.com', NULL, NULL, '$2y$10$fimfbP5J9y/ga2V/gUhNnejS2qNLdMwsoYgIhB15tP/dUB03T9VqS', NULL, 'caf6bf78a3199ba876c7354313ee7c755c306db2', 'owner', '2021-02-06 08:52:29', '2021-02-06 08:52:29', NULL, NULL, NULL),
(11, 'Radja Kasir', 'rizqidino08@gmail.com', NULL, '74401176.jpg', '$2y$10$13mMDA9rUCvh.1ZBMixXHOPQf8CxEIOU4vrva2u7mKxYA/5JRRdwG', NULL, '2c5a14d23405bac6f25de27ce49053f3f57ee3e6', 'super', '2021-02-06 08:54:16', '2021-02-06 08:54:16', NULL, '42342', '5454'),
(13, 'Rizqi Dino', 'rizqidino@unida.gontor.ac.id', NULL, NULL, '2cc3fe13229a0a0bc32883e2385886c2', NULL, NULL, 'pegawai', '2021-04-06 15:39:54', '2021-04-06 15:39:54', '100033023339938515020', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absensi`
--
ALTER TABLE `absensi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gaji`
--
ALTER TABLE `gaji`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gaji_cuti`
--
ALTER TABLE `gaji_cuti`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gaji_lembur`
--
ALTER TABLE `gaji_lembur`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jam_kerja`
--
ALTER TABLE `jam_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kerja`
--
ALTER TABLE `kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `potongan`
--
ALTER TABLE `potongan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_pay`
--
ALTER TABLE `role_pay`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_payment`
--
ALTER TABLE `role_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absensi`
--
ALTER TABLE `absensi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=704;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gaji`
--
ALTER TABLE `gaji`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `gaji_cuti`
--
ALTER TABLE `gaji_cuti`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `gaji_lembur`
--
ALTER TABLE `gaji_lembur`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jam_kerja`
--
ALTER TABLE `jam_kerja`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kerja`
--
ALTER TABLE `kerja`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `potongan`
--
ALTER TABLE `potongan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `role_pay`
--
ALTER TABLE `role_pay`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `role_payment`
--
ALTER TABLE `role_payment`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
