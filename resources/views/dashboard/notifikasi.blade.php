@extends('layouts.main')
<?php $title ='Notifikasi'; 
function rupiah($m)
{
	$rupiah = "Rp ".number_format($m,0,",",".").",-";
	return $rupiah;
}
?>

@section('content')

<div class="content mt-3">

	<div class="col-sm-12 col-lg-12">
		<div class="card text-white bg-flat-color-1">
			<div class="card-body pb-0">
				<div class="dropdown float-right">
					<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">

						<li class="nav-item" role="presentation">
							<a style="color: white;" class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Sudah Bayar</a>
						</li>
						<li class="nav-item" role="presentation">
							<a style="color: white;" class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Menunggu Konfirmasi</a>
						</li>
						<li class="nav-item" role="presentation">
							<a style="color: white;" class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Belum Bayar</a>
						</li>
					</ul>
				</div>

				<h4 class="mb-0">
					<span class="count">{{$hitung_bayar}}</span> orang
				</h4>
				<p class="text-light">User yang sudah bayar</p>

				<div class="chart-wrapper px-0" style="height:70px;" height="70">
					<canvas id="widgetChart1"></canvas>
				</div>

			</div>

		</div>
	</div>

</div>

<!-- role sudah bayar -->
<div class="tab-content" id="pills-tabContent">
	<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
		<div class="content mt-3">
			<div class="col-sm-12 col-lg-12">
				<table  class="table table-striped table-bordered">
					<br>
					<thead>
						<tr>
							<th>No</th>
							<th>Nama</th>
							<th>Email</th>
							<th>Masa Pemakaian</th>
							<th>Status</th>
							
						</tr>
					</thead>
					<tbody>
						@php 
						$nomor =1;
						@endphp
						@foreach($bayar as $abs)
						<tr>
							<td>{{$nomor++}}</td>
							<td>{{$abs->name}}</td>
							<td>{{$abs->email}}</td>
							<!-- abs bayar -->
							@if($abs->dibayar >= now() && $abs->pay == 2 && $abs->level == 'super')
							<td>Selamanya</td>
							<td align="center"><button class="btn btn-success">Super Unlimited</button></td>
							@elseif($abs->dibayar >= now() && $abs->pay == 2 )
							<td>Terlunasi</td>
							<td align="center"><button class="btn btn-success">
								<?php 
								$date = date('Y-m-d', strtotime(now()));
								$date2 = date('Y-m-d', strtotime($abs->dibayar));

								$datetime1 = new DateTime($date);
								$datetime2 = new DateTime($date2);
								$interval = $datetime1->diff($datetime2);
								$days = $interval->format('%a');

								echo $days.' hari lagi';
								?>
							</button></td>
							@elseif($abs->pay ==1)
							<form action="{{ route('users.pay.konfirmasi')}}" method="post">
								@csrf
								<input type="hidden" name="user_id" value="{{$abs->id}}">
								<input type="hidden" name="bulan" value="{{$abs->bulan}}">
								<td>Terlunasi</td>
								<td><button type="submit" class="btn btn-success">Konfirmasi Pembayaran</button></td>
							</form>
							@else
							<td>Belum Terlunasi</td>
							<td >Telah Berakhir</span></td>
							<!-- abs bayar -->
							@endif
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
		<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
			<div class="content mt-3">
				<div class="col-sm-12 col-lg-12">
					<table  class="table table-striped table-bordered">
						<br>
						<thead>
							<tr>
								<th>No</th>
								<th>Nama</th>
								<th>Email</th>
								<th>Masa Pemakaian</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							@php 
							$nomor =1;
							@endphp
							@foreach($konfirmasi as $abs)
							<tr>
								<td>{{$nomor++}}</td>
								<td>{{$abs->name}}</td>
								<td>{{$abs->email}}</td>
								<!-- abs bayar -->
								@if($abs->dibayar >= now() && $abs->pay == 2 && $abs->level == 'super')
								<td>Selamanya</td>
								<td align="center"><button class="btn btn-success">Super Unlimited</button></td>
								@elseif($abs->dibayar >= now() && $abs->pay == 2 )
								<td>Terlunasi</td>
								<td align="center"><button class="btn btn-success">
									<?php 
									$date = date('Y-m-d', strtotime(now()));
									$date2 = date('Y-m-d', strtotime($abs->dibayar));

									$datetime1 = new DateTime($date);
									$datetime2 = new DateTime($date2);
									$interval = $datetime1->diff($datetime2);
									$days = $interval->format('%a');

									echo $days.' hari lagi';
									?>
								</button></td>
								@elseif($abs->pay ==1)
								<form action="{{ route('users.pay.konfirmasi')}}" method="post">
									@csrf
									<input type="hidden" name="user_id" value="{{$abs->id}}">
									<input type="hidden" name="bulan" value="{{$abs->bulan}}">
									<td>Terlunasi</td>
									<td><button type="submit" class="btn btn-success">Konfirmasi Pembayaran</button></td>
								</form>
								@else
								<td>Belum Terlunasi</td>
								<td >Telah Berakhir</span></td>
								<!-- abs bayar -->
								@endif
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
		<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
			<div class="content mt-3">
				<div class="col-sm-12 col-lg-12">
					<table  class="table table-striped table-bordered">
						<br>
						<thead>
							<tr>
								<th>No</th>
								<th>Nama</th>
								<th>Email</th>
								<th>Masa Pemakaian</th>
								<th>Status</th>

							</tr>
						</thead>
						<tbody>
							@php 
							$nomor =1;
							@endphp
							@foreach($belum_bayar as $abs)
							<tr>
								<td>{{$nomor++}}</td>
								<td>{{$abs->name}}</td>
								<td>{{$abs->email}}</td>
								<!-- abs bayar -->
								@if($abs->dibayar >= now() && $abs->pay == 2 && $abs->level == 'super')
								<td>Selamanya</td>
								<td align="center"><button class="btn btn-success">Super Unlimited</button></td>
								@elseif($abs->dibayar >= now() && $abs->pay == 2 )
								<td>Terlunasi</td>
								<td align="center"><button class="btn btn-success">
									<?php 
									$date = date('Y-m-d', strtotime(now()));
									$date2 = date('Y-m-d', strtotime($abs->dibayar));

									$datetime1 = new DateTime($date);
									$datetime2 = new DateTime($date2);
									$interval = $datetime1->diff($datetime2);
									$days = $interval->format('%a');

									echo $days.' hari lagi';
									?>
								</button></td>
								@elseif($abs->pay ==1)
								<form action="{{ route('users.pay.konfirmasi')}}" method="post">
									@csrf
									<input type="hidden" name="user_id" value="{{$abs->id}}">
									<input type="hidden" name="bulan" value="{{$abs->bulan}}">
									<td>Terlunasi</td>
									<td><button type="submit" class="btn btn-success">Konfirmasi Pembayaran</button></td>
								</form>
								@else
								<td>Belum Terlunasi</td>
								<td >Telah Berakhir</span></td>
								<!-- abs bayar -->
								@endif
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection