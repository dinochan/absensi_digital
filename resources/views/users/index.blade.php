@extends('layouts.main')
@section('content')
<?php $title ='User Pegawai'; ?>
@foreach($data as $row)

<div style="text-align: center;" class="modal fade" id="modal-bukti{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h3>Bukti Pembayaran</h3>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					<i class="now-ui-icons ui-1_simple-remove"></i>
				</button>
			</div>
			<div id="#{{$row->id}}" class="modal-body">
				<div class="instruction">
					<div class="row">
						<div class="col-md-12">
							@if($row->image == NULL)
							<h3>Belum Mengirim Bukti Pembayaran</h3>
							@else
							<img width="50%" height="50%" src="{{ URL::to('/') }}/images/{{ $row->image }}">
							<hr>
							<div class="row">
								<label class="col-md-3 col-form-label">Total Pembayaran</label>
								<div class="col-md-9">
									<div class="form-group">
										<input type="text" value="{{ rupiah($row->harga) }}" class="form-control" readonly="" >
									</div>
								</div>
								<label class="col-md-3 col-form-label">Bank</label>
								<div class="col-md-9">
									<div class="form-group">
										<input type="text" value="{{ $row->bank }}" class="form-control" readonly="" >
									</div>
								</div>
								<label class="col-md-3 col-form-label">Tgl Bayar</label>
								<div class="col-md-9">
									<div class="form-group">
										<input type="text" value="{{ $row->tgl_bayar }}" class="form-control" readonly="" >
									</div>
								</div>
								<label class="col-md-3 col-form-label">Masa Pemakaian</label>
								<div class="col-md-9">
									<div class="form-group">
										<input type="text" value="{{ $row->bulan }} bulan" class="form-control" readonly="" >
									</div>
								</div>
							</div>
							@endif
						</div>
					</div>
				</div>
			</div>
			<br><br>
			<div class="modal-footer justify-content-center">
				<form action="{{ route('users.pay.konfirmasi')}}" method="post">
					@csrf
					<input type="hidden" name="user_id" value="{{$row->id}}">
					<input type="hidden" name="bulan" value="{{$row->bulan}}">

					<td><button type="submit" class="btn btn-success">Konfirmasi Pembayaran</button></td>
				</form>
			</div>
		</div>
	</div>
</div>

@endforeach


<!-- modal pembayaran -->
@foreach($data as $row)

<div class="modal fade" id="staticBackdrop-{{$row->id}}" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="staticBackdropLabel">Bayar Aplikasi - {{$row->name}}</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="{{ route('users.pay')}}" method="post" enctype="multipart/form-data">
				@csrf
				<div class="modal-body">
					<h4>Silahkan bayar menurut keadaan finansial hidup anda sekarang.</h4> <br>
					<div class="row">
						<label class="col-md-3 col-form-label">Dari Tanggal</label>
						<div class="col-md-5">
							<div class="form-group">
								<input type="text" name="tgl_bayar" value="{{now()}}" class="form-control" readonly="">
							</div>
						</div>
					</div>
					<input type="hidden" name="user_id" value="{{$row->id}}" class="form-control" readonly="">

					<div class="row">
						@foreach($harga as $a)
						<div class="col-md-2">
							<label class="btn btn-outline-primary ">
								<input type="radio" name="bulan" id="option2" v-model="pilih" value="{{ $a->bulan }}" required=""> 
								<br>Paket {{ $a->bulan }} Bulan
								<br>{{ rupiah($a->harga) }}
							</label>
						</div>

						@endforeach
					</div><br>
					<div class="row">
						<label class="col-md-3 col-form-label">Bukti Transfer</label>
						<div class="col-md-5">
							<div class="form-group">
								<input type="file" name="image" class="form-control" required="">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-outline-success"><h5>Pilih Masa Pemakaian  </h5></button>

				</div>
			</form>
		</div>
	</div>
</div>
@endforeach


<div class="content mt-3">
	<div class="animated fadeIn">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<strong class="card-title">User Pegawai</strong>
						@if(  auth()->user()->level == 'super')
						<a style="float: right;" href="{{route('harga.create')}}" class="btn btn-secondary btn-sm">Buat Harga Aplikasi</a>
						<a style="float: right;" href="{{route('pegawai.create')}}" class="btn btn-primary btn-sm">Tambah User</a>

						@elseif(auth()->user()->level == 'owner' )
						<a style="float: right;" href="{{route('pegawai.create')}}" class="btn btn-primary btn-sm">Tambah User</a>

						@endif
					</div>
					<div class="card-body">
						<table id="bootstrap-data-table-export" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama</th>
									<th>Email</th>
									<th>Total Bayar</th>
									<th>Tgl Bayar</th>
									<th>Payment</th>
									<th>Masa Pemakaian</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@php 
								$nomor =1;
								function rupiah($m)
								{
									$rupiah = "Rp ".number_format($m,0,",",".").",-";
									return $rupiah;
								}
								@endphp
								@foreach($data as $row)
								<tr>
									<td>{{$nomor++}}</td>
									<td>{{$row->name}}</td>
									<td>{{$row->email}}</td>
									<td>{{rupiah($row->harga)}}</td>
									<td>{{$row->tgl_bayar}}</td>

									<!-- row bayar -->
									@if($row->dibayar >= now() && $row->pay == 2 && $row->level == 'super')
									<td>Selamanya</td>
									<td><button class="btn btn-success">Super Unlimited</button></td>
									@elseif($row->dibayar >= now() && $row->pay == 2 )
									<td>Terlunasi</td>
									<td align="center"><button class="btn btn-success">
										<?php 
										$date = date('Y-m-d', strtotime(now()));
										$date2 = date('Y-m-d', strtotime($row->dibayar));

										$datetime1 = new DateTime($date);
										$datetime2 = new DateTime($date2);
										$interval = $datetime1->diff($datetime2);
										$days = $interval->format('%a');

										echo $days.' hari lagi';
										?>
									</button></td>
									@elseif($row->pay ==1)
									<form action="{{ route('users.pay.konfirmasi')}}" method="post">
										@csrf
										<input type="hidden" name="user_id" value="{{$row->id}}">
										<input type="hidden" name="bulan" value="{{$row->bulan}}">
										<td>Terlunasi</td>
										<td><button type="submit" class="btn btn-success">Konfirmasi Pembayaran</button></td>
									</form>
									@else
									<td>Belum Terlunasi</td>
									<td>Telah Berakhir</span></td>
									<!-- row bayar -->
									@endif
									<td class="text-center">
										@if($row->dibayar >= now() && $row->pay == 2 ) 
										<form id="data-{{ $row->id }}" action="{{route('users.destroy',$row->id)}}" method="post">
											{{csrf_field()}}
											{{method_field('delete')}}
										</form>
										@csrf
										@method('DELETE')
										@if(auth()->user()->level == 'owner' or auth()->user()->level == 'super')

										<button type="submit" onclick="deleteRow( {{ $row->id }} )" class="btn btn-round btn-danger btn-icon btn-sm remove"><i class="menu-icon fa fa-trash"></i></button>

										@endif
										@elseif ( $row->pay == 1 ) 
										<a href="{{url('admin/user/'.$row->id.'/edit')}}" data-toggle="modal" data-target="#modal-bukti{{$row->id}}"  class="btn btn-round btn-info btn-icon btn-sm like"><i class="menu-icon fa fa-file"></i></a>
										@else
										<button type="button" data-toggle="modal" data-target="#staticBackdrop-{{$row->id}}" class="btn btn-danger"><i class="now-ui-icons business_money-coins"></i> Bayar Sekarang</button>

										@endif
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div><!-- .animated -->
</div><!-- .content -->
@section('script')

<script type="text/javascript">
	$('[data-countdown]').each(function() {
		var $this = $(this), finalDate = $(this).data('countdown');
		$this.countdown(finalDate, function(event) {
			$this.html(event.strftime('%D hari %H:%M:%S'));
		});
	});
</script>
<script type="text/javascript"> 
	$(document).ready(function () {
		$('#table-datatables').DataTable({
			"scrollX": true,
			lengthMenu: [5, 10, 20, 50, 100, 200, 500, 1000],
			pagingType: 'numbers',
			dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
		});
	});
</script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
	$(document).ready(function() {
		let start = moment().startOf('month')
		let end = moment().endOf('month')

		$('#exportpdf').attr('href', '/administrator/reports/order/pdf/' + start.format('YYYY-MM-DD') + '+' + end.format('YYYY-MM-DD'))

		$('#created_at').daterangepicker({
			startDate: start,
			endDate: end
		}, function(first, last) {
			$('#exportpdf').attr('href', '/administrator/reports/order/pdf/' + first.format('YYYY-MM-DD') + '+' + last.format('YYYY-MM-DD'))
		})
	})
</script>

@endsection
@endsection

