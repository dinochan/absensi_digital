@extends('layouts.main')
@section('content')
<?php $title ='Tabel Cuti User'; ?>

<div class="content mt-3">
	<div class="animated fadeIn">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<strong class="card-title">Tabel Cuti</strong>
					</div>
					<div class="card-body">
						<table id="bootstrap-data-table-export" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama Pegawai</th>
									<th>Tgl Mulai Cuti</th>
									<th>Tgl Akhir Cuti</th>
									<th>Nominal Cuti(tidak diambil)</th>
									<th>Pengajuan Cuti</th>
									<th>Cuti Terambil</th>
								</tr>
							</thead>
							<tbody>
								@php 
								$nomor =1;
								function rupiah($m)
								{
									$rupiah = "Rp ".number_format($m,0,",",".").",-";
									return $rupiah;
								}
								@endphp
								@foreach($cuti as $abs)
								<?php 
								$tgl_mulai = date( "l, d F Y", strtotime($abs->tgl_mulai_cuti));
								$tgl_akhir = date( "l, d F Y", strtotime($abs->tgl_akhir_cuti));
								?>
								<tr>
									<td>{{$nomor++}}</td>
									<td>{{$abs->name}}</td>
									<td>{{$tgl_mulai}}</td>
									<td>{{$tgl_akhir}}</td>
									<td>{{rupiah($abs->gaji)}}</td>
									<td>{{$abs->hari}} hari</td>
									<td>{{$abs->cuti}} hari</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div><!-- .animated -->
</div><!-- .content -->
@endsection
