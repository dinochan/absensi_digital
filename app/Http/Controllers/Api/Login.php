<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\User;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
class Login extends Controller
{
	public function login(Request $request)
	{
		$credentials = $request->only('email', 'password');
		if (Auth::attempt($credentials)) {
			$data = DB::table('users')
			->where('email' ,'=' , $credentials)
			->first();
			return response()->json([
				'username' => $data,
				'status_code'   => 200,
				'msg'           => 'success',
			], 200);
		}else{
			return response()->json([
				'username' => 'tidak ditemukan',
				'status_code'   => 200,
				'msg'           => 'not found',
			], 200);
		}
	}
	public function register(Request $request){
		$data = DB::table('users')
		->where('email' ,'=' , $request->email)
		->first();
		if($data == FALSE){
			$data = new User;
			$data->name = $request->name;
			$data->email = $request->email;
			$data->password = Hash::make($request->password);
			$data->id_team = bin2hex(random_bytes(20));
			$data->save();

			DB::table('perusahaan')
			->insert([
				'id_team' =>  $data['id_team'],
				'nama_perusahaan' =>  $request->nama_perusahaan,
				'lokasi' =>  $request->lokasi,
				'alamat' =>  $request->alamat,
				'longitude' =>  $request->longitude,
				'latitude' =>  $request->latitude,
				'created_at' =>  now(),
				'updated_at' =>  now(),
				
			]);
			DB::table('role_payment')
			->insert([
				'user_id' =>  $data['id'],
				'pay' =>  2,
				'dibayar' => date('Y-m-d', strtotime('+7 days', strtotime(now())))
			]);
			return response()->json([
				'users' => $data,
				'status_code'   => 200,
				'msg'           => 'Email disimpan',
			], 200); 
		}elseif($data == TRUE){
			return response()->json([
				'users' => 'Not Success',
				'status_code'   => 409,
				'msg'           => 'Email Sudah Ada',
			], 200); 
		}


	}
	public function koordinat_all()
	{
		$data = DB::table('users')
		->select([
			'id',
			'name',
			'email',
			'longitude',
			'latitude',
		])
		->get();
		return response()->json([
			'koordinat_semua_pegawai' => $data,
			'status_code'   => 200,
			'msg'           => 'success',
		], 200);
	}
	public function koordinat(Request $request)
	{
		$data = DB::table('users')
		->where('users.id', $request->user_id)
		->select([
			'id',
			'name',
			'email',
			'longitude',
			'latitude',
		])
		->get();
		return response()->json([
			'koordinat_pegawai' => $data,
			'status_code'   => 200,
			'msg'           => 'success',
		], 200);
	}
	public function koordinat_edit(Request $request)
	{
		$data = DB::table('users')
		->where('users.id', $request->user_id)
		->update([
			'longitude' => $request->longitude,
			'latitude' => $request->latitude,
		]);
		return response()->json([
			'koordinat_pegawai_edit' => $data,
			'status_code'   => 200,
			'msg'           => 'success',
		], 200);
	}
	
}
