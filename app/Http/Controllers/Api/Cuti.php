<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class Cuti extends Controller
{
	public function index(Request $request)
	{
		$data = DB::table('gaji_cuti')
		->join('users', 'gaji_cuti.user_id', 'users.id')
		->where('users.id_team', $request->id_team)
		->get();
		return response()->json([
			'tabel_cuti' => $data,
			'status_code'   => 200,
			'msg'           => 'success',
		], 200);
	}
	public function user(Request $request)
	{
		$data = DB::table('gaji_cuti')
		->join('users', 'gaji_cuti.user_id', 'users.id')
		->where('gaji_cuti.user_id', $request->user_id)
		->get();
		return response()->json([
			'tabel_cuti_user' => $data,
			'status_code'   => 200,
			'msg'           => 'success',
		], 200);
	}
	public function ajukan(Request $request)
	{
		$user = DB::table('gaji_cuti')
		->where('user_id', $request->user_id)
		->first();
		if ($user === NULL) {
			$data = DB::table('gaji_cuti')
			->insert([
				'user_id' => $request->user_id,
				'tgl_mulai_cuti' => $request->tgl_mulai,
				'tgl_akhir_cuti' => $request->tgl_akhir,
				'hari' => $request->hari,
				'keperluan' => $request->keperluan,
				'created_at' =>now(),
				'updated_at' =>now(),
			]);
			return response()->json([
				'ajukan_cuti' => $data,
				'status_code'   => 200,
				'msg'           => 'success',
			], 200);
		}else{
			$data = DB::table('gaji_cuti')
			->where('gaji_cuti.user_id', $request->user_id)
			->update([
				'user_id' => $request->user_id,
				'tgl_mulai_cuti' => $request->tgl_mulai,
				'tgl_akhir_cuti' => $request->tgl_akhir,
				'hari' => $request->hari,
				'keperluan' => $request->keperluan,
				'created_at' =>now(),
				'updated_at' =>now(),
			]);
			return response()->json([
				'ajukan_cuti' => $data,
				'status_code'   => 200,
				'msg'           => 'success',
			], 200);
		}
		
	}
	public function delete_ajukan(Request $request)
	{
		$data = DB::table('gaji_cuti')
		->where('gaji_cuti.user_id', $request->user_id)
		->delete();
		return response()->json([
			'ajukan_cuti_user_hapus' => $data,
			'status_code'   => 200,
			'msg'           => 'success',
		], 200);
	}
	public function bonus(Request $request)
	{
		$data = DB::table('gaji_cuti')
		->where('gaji_cuti.user_id', $request->user_id)
		->update([
			'gaji' => $request->gaji,
		]);
		return response()->json([
			'cuti_beri_bonus' => $data,
			'status_code'   => 200,
			'msg'           => 'success',
		], 200);
	}
	public function approve(Request $request)
	{
		$data = DB::table('gaji_cuti')
		->where('gaji_cuti.user_id', $request->user_id)
		->update([
			'status' => 1,
		]);
		return response()->json([
			'cuti_approved' => $data,
			'status_code'   => 200,
			'msg'           => 'success',
		], 200);
	}
	public function unapprove(Request $request)
	{
		$data = DB::table('gaji_cuti')
		->where('gaji_cuti.user_id', $request->user_id)
		->update([
			'status' => 0,
		]);
		return response()->json([
			'cuti_unapproved' => $data,
			'status_code'   => 200,
			'msg'           => 'success',
		], 200);
	}
}
