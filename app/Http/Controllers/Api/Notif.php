<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB; 
class Notif extends Controller
{
	public function pembayaran(Request $request)
	{
		$bayar = DB::table('users')
		->join('role_payment', 'users.id', '=', 'role_payment.user_id')
		->where('role_payment.pay', 2)
		->where('role_payment.dibayar', '>=', now())
		->get();

		$konfirmasi = DB::table('users')
		->join('role_payment', 'users.id', '=', 'role_payment.user_id')
		->where('role_payment.pay', 1)
		->get();

		$belum_bayar = DB::table('users')
		->join('role_payment', 'users.id', '=', 'role_payment.user_id')
		->where('role_payment.dibayar', '<=', now())
		
		->get();

		$hitung_bayar = DB::table('users')
		->join('role_payment', 'users.id', '=', 'role_payment.user_id')
		->where('role_payment.pay', 2)
		->count();

		return response()->json([
			'notif_belum_bayar' => $belum_bayar,
			'notif_sudah_bayar' => $bayar,
			'notif_menunggu_konfirmasi' => $konfirmasi,
			'status_code'   => 200,
			'msg'           => 'success',
		], 200);
	}
	public function pay_konfirmasi(Request $request)
	{
		$waktu = ($request->bulan * 30)." days" ;

		$data = DB::table('role_payment')
		->where('user_id', '=', $request->user_id)
		->update([
			'pay'  => 2,
			'dibayar'  => date('Y-m-d', strtotime($waktu, strtotime(now()))),
		]);
		return response()->json([
			'pay' => $data,
			'status_code'   => 200,
			'msg'           => 'success',
		], 200);
	}
	public function user_status(Request $request)
	{
		$data = DB::table('role_payment')->where('user_id', $request->user_id)->get();
		return response()->json([
			'status_user' => $data,
			'status_code'   => 200,
			'msg'           => 'success',
		], 200);
	}
	public function harga()
	{
		$data = DB::table('role_pay')->get();
		return response()->json([
			'harga_pemakaian' => $data,
			'status_code'   => 200,
			'msg'           => 'success',
		], 200);
	}
	public function pay_user(Request $request)
	{
		$image = $request->file('image');
		$image_name = rand() . '.' . $image->getClientOriginalExtension();
		$image->move(public_path('images'), $image_name);

		$data = DB::table('role_payment')
		->where('role_payment.user_id', $request->user_id)
		->update([
			'role_payment.tgl_bayar'  => now(),
			'role_payment.harga'  => $request->harga,
			'role_payment.bulan'  => $request->bulan,
			'role_payment.image'  => $image_name,
			'role_payment.created_at'  => now(),
			'role_payment.updated_at'  => now(),
		]);
		return response()->json([
			'bayar' => $data,
			'status_code'   => 200,
			'msg'           => 'success',
		], 200);
	}
}
